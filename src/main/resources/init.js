
function objToString(obj) {
    if (obj == null) {
        return null;
    }
    if (typeof obj === 'object') {
        return JSON.stringify(obj);
    } else {
        return obj;
    }
}

function concat() {
    var s = "";
    for (var i = 0; i < arguments.length; i++) {
        s += arguments[i]
    }
    return s
}

function cas(obj, from, to) {
    if (obj === from) {
        return to;
    } else {
        return obj;
    }
}

function extract(obj, regex) {
    if (!obj) {
        return obj;
    }
    var reg = new RegExp(regex);
    var s = obj.toString();
    var list = s.match(reg);
    if (list && list.length > 0) {
        return list[0];
    } else {
        return null;
    }
}

function indexOf(arr, obj) {
    return arr.indexOf(obj);
}

var _globalIndex = 0;

function $index(n) {
    if (n == null) {
        if (_index != null) {
            return _index + "";
        } else {
            return (_globalIndex++) + "";
        }
    }
    if (_index != null) {
        return n + _index + "";
    } else {
        return (_globalIndex++) + "";
    }
}


var _blankArgCacheMap = {};

var _oneArgCacheMap = {};

function _clearCache() {
    _blankArgCacheMap = {};
    _oneArgCacheMap = {};
    _java.clearCache();
}

function _cachePut(args, result) {
    if (args.length === 0) {
        _blankArgCacheMap[args.callee.name] = result;
        return result
    }
    if (args.length === 1) {
        var map = _oneArgCacheMap[args.callee.name];
        if (map == null) {
            map = {};
            _oneArgCacheMap[args.callee.name] = map;
        }
        map[args[0]] = result;
        return result;
    }
    var arg = [];
    for (var i = 0; i < args.callee.length; i++) {
        arg[i] = args[i];
    }
    _java.cachePut(args.callee.name, arg, result);
    return result
}

function _cacheGet(args) {
    if (args.length === 0) {
        return _blankArgCacheMap[args.callee.name]
    }
    if (args.length === 1) {
        var map = _oneArgCacheMap[args.callee.name];
        if (map == null) {
            return null;
        }
        return map[args[0]];
    }
    var arg = [];
    for (var i = 0; i < args.callee.length; i++) {
        arg[i] = args[i];
    }
    return _java.cacheGet(args.callee.name, arg);
}

function reversal(list) {
    var r = _cacheGet(arguments);
    if (r != null) {
        return r;
    }
    if (list == null) {
        return null;
    }
    r = [];
    for (var i = 0; i < list.length; i++) {
        r[i] = list[list.length - 1 - i];
    }
    return _cachePut(arguments, r);
}

function array(len, value) {
    var r = _cacheGet(arguments);
    if (r != null) {
        return r;
    }
    r = [];
    if (!len) {
        return r;
    }
    if (value == null) {
        value = '';
    }
    for (var i = 0; i < len; i++) {
        r[i] = value;
    }
    return _cachePut(arguments, r);
}

function align(list, value) {
    var r = _cacheGet(arguments);
    if (r != null) {
        return r;
    }
    if (list == null) {
        return null;
    }
    if (value == null) {
        value = ""
    }
    r = [];
    for (var i = 0; i < list.length; i++) {
        r[i] = value;
    }
    return _cachePut(arguments, r);
}

function fill(arr, value) {
    var r = _cacheGet(arguments);
    if (r != null) {
        return r;
    }
    if (arr == null) {
        return arr;
    }
    if (value == null) {
        value = '';
    }
    for (var i = 0; i < arr.length; i++) {
        if (!arr[i]) {
            arr[i] = value;
        }
    }
    return _cachePut(arguments, arr);
}

function change(arr, from, to) {
    var r = _cacheGet(arguments);
    if (r != null) {
        return r;
    }
    if (arr == null) {
        return arr;
    }
    if (from == null) {
        return arr;
    }
    if (to == null) {
        to = '';
    }
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] === from) {
            arr[i] = to;
        }
    }
    return _cachePut(arguments, arr);
}