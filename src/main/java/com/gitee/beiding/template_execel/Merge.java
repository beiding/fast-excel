package com.gitee.beiding.template_execel;


import java.util.HashMap;
import java.util.Map;

class Merge {

    //跨行
    private int row;

    //跨列
    private int col;

    private static Map<String, Merge> map = new HashMap<>();

    //唯一的
    static Merge get(int row, int col) {
        String name = row + ":" + col;
        return map.computeIfAbsent(name, k -> new Merge(row, col));
    }

    private Merge(int row, int col) {
        this.row = row;
        this.col = col;
    }

    int getRow() {
        return row;
    }

    int getCol() {
        return col;
    }
}
