package com.gitee.beiding.template_execel;


import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * 使用该类添加全局配置,例如日期格式化等
 */
public class Config {

    private static DateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static void setDataFormat(String format) {
        dataFormat = new SimpleDateFormat(format);
    }

    public static DateFormat getDataFormat() {
        return dataFormat;
    }


    //js工作器数量
    private static int coreJsWorkerNumber = 5;

    public static void setCoreJsWorkerNumber(int coreJsWorkerNumber) {
        Config.coreJsWorkerNumber = coreJsWorkerNumber;
    }

    public static int getCoreJsWorkerNumber() {
        return coreJsWorkerNumber;
    }
}
