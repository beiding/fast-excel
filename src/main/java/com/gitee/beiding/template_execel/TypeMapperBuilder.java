package com.gitee.beiding.template_execel;

import java.util.HashMap;
import java.util.Map;

public class TypeMapperBuilder {

    private Map<String, Class<?>> mapping = new HashMap<>();

    public Map<String, Class<?>> toMap() {
        return mapping;
    }

    public TypeMapperBuilder mapping(String name, Class<?> t) {
        this.mapping.put(name, t);
        return this;
    }

    public static TypeMapperBuilder createAndMapping(String name, Class<?> t) {
        return new TypeMapperBuilder().mapping(name, t);
    }
}
