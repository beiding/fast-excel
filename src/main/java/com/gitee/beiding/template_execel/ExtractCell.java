package com.gitee.beiding.template_execel;

        import java.util.Collections;
        import java.util.Map;

 abstract class ExtractCell {


     int blankToken = 0;

     Merge merge;

    void setMerge(Merge merge) {
        this.merge = merge;
    }


    void setToken(int blankToken) {
        this.blankToken = blankToken;
    }

    boolean takeToke() {
        if (blankToken > 0) {
            blankToken--;
            return true;
        }
        return false;
    }

    static Map<String, ValueHolder> emptyMap = Collections.emptyMap();

    //统一使用Object接受参数
    abstract Map<String, ValueHolder> extract(Object obj, Merge merge);

}
