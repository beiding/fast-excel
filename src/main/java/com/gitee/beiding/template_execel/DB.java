package com.gitee.beiding.template_execel;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

class DB {

    //所有字段
    private Set<String> cols;

    private Map<String, Object> map = new HashMap<>();

    DB(Set<String> cols) {
        this.cols = cols;
    }

    private String conditionToKey(Map<String, Integer> condition) {
        return cols.stream().map((k) -> k + "=" + condition.get(k)).collect(Collectors.joining());
    }

    //使用and条件查询结果
    Object andValue(Map<String, Integer> condition) {

        String key = conditionToKey(condition);
        if (map.containsKey(key)) {
            return map.get(key);
        }

        throw InvalidIndexException.INSTANCE;

    }

    Object putValue(Map<String, Integer> condition, Object value) {
        return map.put(conditionToKey(condition), value);
    }


}
