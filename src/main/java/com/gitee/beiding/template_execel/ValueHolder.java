package com.gitee.beiding.template_execel;

class ValueHolder {

    ValueHolder(Object value, int row) {
        this.value = value;
        this.row = row;
    }

    //值
    private Object value;

    void setValue(Object value) {
        this.value = value;
    }

    //值的跨度
    private int row;

    int getRow() {
        return row;
    }

    Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value + "   " + row;
    }
}
