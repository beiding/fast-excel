package com.gitee.beiding.template_execel;

//模板单元


import java.util.*;

class TemplateCell {
    static TemplateCell parse(Object s) {


        TemplateCell r = new TemplateCell();

        if (s instanceof String) {

            TemplateCellParseUtils.Result array = TemplateCellParseUtils.parse((String) s);
            LinkedHashMap<String, LinkedHashSet<String>> map = array.getCheckJsMap();

            if (map.size() > 0) {

                //初始化
                r.arrayIndex = new ArrayIndex();
                r.arrayIndex.init(map);
                r.indexSet = map.keySet();

                //初始化?
                r.indexMax = new HashMap<>();

                for (String index : r.indexSet) {
                    r.indexMax.put(index, 0);
                }

            }
            //放入表达式
            r.expression = array.getExpression();
            r.commands = array.getCommands();

        } else {
            r.singleResult = s;
        }
        return r;
    }

    private TemplateCell() {

    }

    private List<String> commands;

    //单元格表达式
    private String expression;

    //数组索引
    private ArrayIndex arrayIndex;

    private Set<String> indexSet;

    private Map<String, Integer> indexMax;

    private Object singleResult;

    private DB db;


    Map<String, Integer> getIndexMax() {
        return indexMax;
    }

    Object getSingleResult() {
        return singleResult;
    }

    //运算
    void exe() throws Exception {

        if (expression != null) {

            Map<String, Integer> condition = new HashMap<>();

            if (arrayIndex != null) {

                db = new DB(indexSet);

                while (arrayIndex.autoIncrement()) {

                    for (String s : indexMax.keySet()) {
                        int v = (Integer) Js.get(s);
                        Integer currentMax = indexMax.get(s);
                        if (currentMax == null || v > currentMax) {
                            indexMax.put(s, v);
                        }
                        condition.put(s, v);
                    }

                    //放入结果集
                    db.putValue(condition, Js.exe(expression));

                }

                //清空临时变量
                arrayIndex.clear();

            } else {
                //直接运算得到结果
                singleResult = Js.exe(expression);
            }
        }

    }

    /*
      建立索引
     */
    Object get(Map<String, Integer> index) {

        Object r = null;

        if (arrayIndex == null) {
            r = singleResult;
        } else {
            r = db.andValue(index);
        }


        if (commands != null) {

            if (r == null) {
                return null;
            }

            String s = r.toString();

            for (String command : commands) {
                try {
                    Object exe = Js.exe(command);
                    if (exe == null) {
                        s = s.replace(command, "");
                    } else {
                        s = s.replace(command, exe.toString());
                    }
                } catch (Exception e) {
                }
            }

            return s;

        }

        return r;

    }


}

