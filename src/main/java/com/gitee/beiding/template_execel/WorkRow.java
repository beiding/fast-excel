package com.gitee.beiding.template_execel;

import java.util.*;

/*

    该类只对单元格内容进行判断已经提取并不会处理单元格合并

    提取时从前向后进行

 */
class WorkRow {

    //对应的列以及提取单元
    private Map<Integer, ExtractCell> extractCellMap = new HashMap<>();

    //判断是否能够首位对齐
    private int firstCol;

    private int lastCol;

    void setFirstCol(int firstCol) {
        this.firstCol = firstCol;
    }

    int getFirstCol() {
        return firstCol;
    }

    void setLastCol(int lastCol) {
        this.lastCol = lastCol;
    }

    int getLastCol() {
        return lastCol;
    }

    private Map<String, List<ValueHolder>> colValueMap;

    //必须手动放入一个容器
    WorkRow(Map<String, List<ValueHolder>> colValueMap) {
        this.colValueMap = colValueMap;
    }

    //编译提取单元
    void compile(int col, Object value, Merge merge) {//编译的时候同时放入一个单元格合并
        //不存在空白的模板行和数据行
        if (value instanceof String) {
            String exp = (String) value;
            RegexExtractCell compile = RegexExtractCell.compile(exp);
            compile.setMerge(merge);
            extractCellMap.put(col, compile);
        } else {
            ValueExtractCell valueExtractCell = new ValueExtractCell(value);
            valueExtractCell.setMerge(merge);
            extractCellMap.put(col, valueExtractCell);
        }

    }


    //对本行中的单元格进行解析
    boolean extract(int col, Object value, Merge merge) {

        ExtractCell cell = extractCellMap.get(col);

        //首先尝试获取token,如果能够获取token直接返回
        if (cell.takeToke()) {
            return true;
        }

        Map<String, ValueHolder> map = cell.extract(value, merge);

        //如果无法提取到内容直接返回false
        if (map == null) {
            return false;
        }

        //将提取到的数据加入到结果集中
        if (map.size() > 0) {
            //获取所有的值
            for (String k : map.keySet()) {
                ValueHolder holder = map.get(k);
                List<ValueHolder> valueHolders = colValueMap.computeIfAbsent(k, kk -> new LinkedList<>());
                valueHolders.add(holder);
            }
        }
        return true;
    }


}
