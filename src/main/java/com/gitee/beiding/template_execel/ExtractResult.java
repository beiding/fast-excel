package com.gitee.beiding.template_execel;

import java.util.Map;

//结果是只读的
public class ExtractResult {

    private Map<String, Object> data;

    ExtractResult(Map<String, Object> data) {
        this.data = data;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public Object get(String name) {
        return data.get(name);
    }

    public <T> T get(String name, Class<T> tClass) {
        return (T) data.get(name);
    }

}
